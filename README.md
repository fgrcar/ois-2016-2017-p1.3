# OIS 2016/2017

Podrobna navodila so na voljo v [skripti pri predmetu](https://sandbox.lavbic.net/teaching/OIS/2016-2017/Odjemalec-streznik-in-Node-js.html).

## Kratek uvod v Node.js

### Enostavni spletni strežnik

Hello World; testiranje in ponovni zagon; razvoj lokalno vs. Cloud9; spremenljivke na strežniku.

### Napredni spletni strežnik

Strežnik statičnih spletnih strani; uporaba dodatnih knjižnic; obvladovanje zahtev po virih, ki ne obstajajo; predpomnjenje; dinamične strani; pridobivanje podatkov uporabnika; shranjevanje v datoteko.